#!/usr/bin/python3
"""
For plotting fixed rate result
Use [-h] to see more info and how to use
"""

import itertools
import math
from argparse import ArgumentParser, RawTextHelpFormatter
from subprocess import call

import matplotlib.pyplot as plt
import pandas as pd

pd.options.display.float_format = '{:,.2f}'.format
pd.option_context('display.max_rows', None, 'display.max_columns', 10)

MCS_NUM = 10
MARKER = itertools.cycle(('o', 'v', '^', '*', '+', '.'))
COLOR = itertools.cycle(('b', 'g', 'r', 'orange'))


def argparse():
    """
    add positional or optional arguments
    """
    des = \
        "Plot one statistic of all rates: \n" + \
        "1. from csv: -f *.csv \n" + \
        "2. from mongodb: can have no arguments or -m db_name col_name\n" + \
        "\n" + \
        "Plot many statistics for one ss+bw: \n" + \
        "1. the dataframe can from csv or mongodb \n" + \
        "2. use -c <ss> <bw> <key1> <key2> <key...>\n" + \
        "\n" + \
        "Choose different key by -k:\n" + \
        "SUCCESS, ATTEMPT, LOSS, AMPDU, TX_BYTES, AIRTIME, ETXTIME, AIR_RATIO, PCT, AGG, RSSI, TPT, \n" + \
        "O_TPT, O_ETX, O_AIR, O_AGG, O_RSSI, O_TPT2\n"

    argpar = ArgumentParser(
        description=des, formatter_class=RawTextHelpFormatter)
    argpar.add_argument(
        '-f', '--file', help='a csv file containing dataframe', dest='file', default=None)
    argpar.add_argument(
        '-k', '--key', help='target key for plotting', dest='key', action='append', default=['TPT'])
    argpar.add_argument(
        '-s', '--save-figure', help='save the plotted figure', dest='saveflag', default=False)
    argpar.add_argument(
        '-m', '--mongo', help='define database and collection of mongodb', dest='mongo', nargs=2, default=(None, None))
    argpar.add_argument(
        '-c', '--compare', help='compare multiple keys in one ss+bw, use -c ss, bw, key1, key2,...',
        dest='compare', nargs='+', default=None)

    return argpar


def plot_one_key_all_ss(df_data, key, saveflag):
    """
    Plot one key with all ss+bw
    """
    bw_set = sorted(set(df_data['BW']), reverse=True)
    ss_set = sorted(set(df_data['SS']))
    fig = plt.figure()
    ax1 = fig.add_subplot(111)

    for _b in bw_set:
        for _s in ss_set:
            df_tmp = df_data[(df_data['BW'] == _b) & (df_data['SS'] == _s)]
            label = str(_s) + '_' + str(_b)
            ax1.plot(df_tmp['mcs'], df_tmp[key], '--',
                     marker=next(MARKER), color=next(COLOR), label=label)

    ax1.ticklabel_format(style='sci', scilimits=(-3, 4), axis='both')
    ax1.set_xticks(range(0, MCS_NUM))
    ax1.set_title(key + '_' + df_data['CASE'][0])
    ax1.legend(prop={'size': 10}, ncol=2)

    fig.tight_layout()
    imgname = df_data['CASE'][0] + '_' + df_data['CASE'][0] + '_' + key
    if saveflag:
        fig.savefig(imgname + '.png', format='png', dpi=300)
        print('Saving figure "{0}".\n'.format(imgname + '.png'))
        call(['xdg-open', imgname + '.png'])
    else:
        plt.show()


def plot_many_key_one_ss(df_data, compare, saveflag):
    """
    Plot many keys for one ss+bw
    """
    target_ss = compare[0]
    target_bw = int(compare[1])
    keys = compare[2:]
    N = len(keys)
    row = math.ceil(N/2)
    col = 1 if N == 1 else 2
    df_tmp = df_data[(df_data['BW'] == target_bw) &
                     (df_data['SS'] == target_ss)]

    fig = plt.figure()

    for i in range(N):
        if i & 1:
            ax = plt.subplot2grid((row, col), (i//2, 1), rowspan=1, colspan=1)
        else:
            ax = plt.subplot2grid((row, col), (i//2, 0), rowspan=1, colspan=1)

        ax.plot(df_tmp['MCS'], df_tmp[keys[i]], '--',
                marker=next(MARKER), color=next(COLOR), label=keys[i])
        ax.set_xticks(range(0, MCS_NUM))
        ax.legend(prop={'size': 10}, ncol=2)

    fig.suptitle(target_ss + '_' + str(target_bw))
    fig.tight_layout()
    imgname = target_ss + '_compare'
    if saveflag:
        fig.savefig(imgname + '.png', format='png', dpi=300)
        print('Saving figure "{0}".\n'.format(imgname + '.png'))
        call(['xdg-open', imgname + '.png'])
    else:
        plt.show()


def main():
    """
    Get dataframe from:
    1. csv file
    2. mongodb
    """

    argpar = argparse()
    args = argpar.parse_args()

    if args.file is not None:
        df_data = pd.read_csv(args.file)
    else:
        mongo = Mongo()
        mongo.select_db(args.mongo[0])
        data = mongo.find_col(
            col_name=args.mongo[1], filter1=None, filter2={'_id': 0})
        df_data = pd.DataFrame.from_dict(data[0])
        df_data.index = df_data.index.map(int)
        df_data.sort_index(inplace=True)

    if args.compare is None:
        for key in args.key:
            print(key)
            plot_one_key_all_ss(df_data, key, args.saveflag)

    else:
        assert df_data['SS'].isin([args.compare[0]]).any()  # check SS
        assert df_data['BW'].isin([int(args.compare[1])]).any()  # check BW
        print(args.compare)
        plot_many_key_one_ss(df_data, args.compare, args.saveflag)


if __name__ == '__main__':
    main()
