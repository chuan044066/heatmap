#!/usr/bin/python3
"""
For plotting fixed rate result
Use [-h] to see more info and how to use

python3 plot_heatmap.py -f /Users/chuan/csv/0620_csv(dirname) -k LOSS  -name loss -s 1
"""

import itertools
import math
from argparse import ArgumentParser, RawTextHelpFormatter
from subprocess import call
import os
import matplotlib as mpl
mpl.use('Agg')

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np
pd.options.display.float_format = '{:,.2f}'.format
pd.option_context('display.max_rows', None, 'display.max_columns', 10)

MCS_NUM = 8
MARKER = itertools.cycle(('o', '*', '^'))
COLOR = itertools.cycle(('blue', 'g', 'r','steelblue', 'limegreen','salmon'))


def argparse():
    """
    add positional or optional arguments
    """
    des = \
        "Plot one statistic of all rates: \n" + \
        "1. from csv: -f *.csv \n" + \
        "2. from mongodb: can have no arguments or -m db_name col_name\n" + \
        "\n" + \
        "Plot many statistics for one ss+bw: \n" + \
        "1. the dataframe can from csv or mongodb \n" + \
        "2. use -c <ss> <bw> <key1> <key2> <key...>\n" + \
        "\n" + \
        "Choose different key by -k:\n" + \
        "SUCCESS, ATTEMPT, LOSS, AMPDU, TX_BYTES, AIRTIME, ETXTIME, AIR_RATIO, PCT, AGG, RSSI, TPT, \n" + \
        "O_TPT, O_ETX, O_AIR, O_AGG, O_RSSI, O_TPT2\n"

    argpar = ArgumentParser(
        description=des, formatter_class=RawTextHelpFormatter)
    argpar.add_argument(
        '-f', '--file', help='a csv file containing dataframe', dest='file', default=None)
    argpar.add_argument(
        '-f2', '--file2', help='a csv file containing dataframe', dest='file2', default=None)
    argpar.add_argument(
        '-k', '--key', help='target key for plotting', dest='key', action='store', default=['TPT'])
    argpar.add_argument(
        '-s', '--save-figure', help='save the plotted figure', dest='saveflag', default=False)
    argpar.add_argument(
        '-m', '--mongo', help='define database and collection of mongodb', dest='mongo', nargs=2, default=(None, None))
    argpar.add_argument(
        '-c', '--compare', help='compare multiple keys in one ss+bw, use -c ss, bw, key1, key2,...',
        dest='compare', nargs='+', default=None)
    argpar.add_argument(
        '-name', '--name-figure', help='name the plotted figure', dest='name', default=False)
    argpar.add_argument(
        '-date', '--date', help='dir the plotted figure', dest='date', default=0)
    argpar.add_argument(
        '-date2', '--date2', help='dir the plotted figure', dest='date2', default=False)
    argpar.add_argument(
        '-start', '--start_meter', help='start the plotted figure', dest='start_meter', default=0)
    return argpar


def plot_heatmap(df_data, key, saveflag, name, idx_label, date):
    """
    Plot one key with all ss+bw
    """
    #bw_set = sorted(set(df_data['BW']), reverse=True)
    #ss_set = sorted(set(df_data['SS']))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    stream = {'1':'SS', '2':'DS', '3':'TS'}
    if(key == 'LOSS'):
        fmt_t = '.2f'
    else:
        fmt_t = '.0f'
    ax = sns.heatmap(df_data, annot=True, ax = ax, yticklabels=idx_label, fmt=fmt_t, 
                                annot_kws={'size':9})
    label_y = ax.get_yticklabels()
    label_x = ax.get_xticklabels()
    plt.setp(label_y, rotation=30)
    plt.setp(label_x, rotation=25)
    ax.set_xlabel('distance', fontsize=14)
    ax.set_ylabel('idx     ', rotation=0, fontsize=14)
    if name:
        ax.set_title(name)
    
    #ax1.legend(prop={'size': 10}, ncol=2)
    #ax1.set_xlabel('MCS', fontsize=16)
    #ax1.set_ylabel(key+'(Mbps)', fontsize=16)

    fig.tight_layout()
    if saveflag:
        #fig.savefig(imgname + '.png', format='png', dpi=300)
        fig.savefig(date + '_' + name + '.png', format='png', dpi=300)
        print('Saving figure "{0}".\n'.format(name + '.png'))
    else:
        plt.show()


#Find all the csv file in the dir and set to one pd
def read_all_to_onepd(dirpath, key, start_meter, date):
    df=pd.DataFrame(np.empty([12, 0]))
    for i in range(13):
        m = int(start_meter)+i
        file_name = date + '_' + str(m) + 'm.csv'
        file_name = os.path.join(dirpath, file_name)
        df_tmp = pd.read_csv(file_name)
        col_name = str(m) + 'm'
        df.insert(loc=0, column=col_name, value=df_tmp[key])
    idx_label = df_tmp['idx']
    return df, idx_label


def main():
    """
    Get dataframe from:
    1. csv file
    """

    argpar = argparse()
    args = argpar.parse_args()
    print(args.key)
    if args.file is not None:
        df_data, idx_label = read_all_to_onepd(args.file, args.key, args.start_meter, args.date)
        
    if(args.date2):
        df_data_2, idx_label = read_all_to_onepd(args.file2, args.key, args.start_meter, args.date2)
        df_data = (df_data + df_data_2) / 2
    print(df_data)
    # print((df_data.loc[2]+df_data.loc[6]+df_data.loc[9])/3)
    # print((df_data.loc[7]+df_data.loc[10]+df_data.loc[11])/3)
    # for idx, row in df_data.iteritems():
    #     print(idx)
    #     amount = 0
    #     cnt = 0
    #     for i in range(12):
    #         if(row[i] > 0.05) and (row[i] < 0.95):
    #             cnt += 1
    #             amount += row[i]
    #     print(amount/cnt)
    
    plot_heatmap(df_data, args.key, args.saveflag, args.name, idx_label, args.date)



if __name__ == '__main__':
    main()
