import os
import sys
import csv

first = "idx,SS,BW,sgi,mcs,attemp,success,tx_bytes,airtime,esttime,rssi_sum,rssi_count,LOSS,RSSI,TPT,CASE\n"
# read 0~87 and wrtie to one csv file

if __name__=='__main__':
    _dir = sys.argv[1]
    _file = _dir + ".csv"
    fw = open(_file, 'a')
    wr = csv.writer(fw, dialect='excel')
    fw.write(first)
    for i in range(0,88):
        idx = i
        record = os.path.abspath(_dir)
        record = os.path.join(_dir, str(idx))
        #:qprint(record)
        try:
            fr = open(record, 'r')
        except:
            continue;
        lines = fr.readlines()
        for line in lines:
            _line = line.strip('\n').split(',')
            if(_line[0] == str(idx)):
                success = int(_line[6])
                attemp = int(_line[5])
                LOSS = 1 - round(success / max(attemp,1), 2)
                RSSI = round(int(_line[10])/int(_line[11]), 1)
                ex_time = int(_line[9])
                TPT = round(int(_line[7])*8/ex_time, 1)
                _line.append(LOSS)
                _line.append(RSSI)
                _line.append(TPT)
                _line.append(_dir)
                wr.writerow(_line)
                #print(line)
                #fw.write(line)
    
    
    fw.close()


